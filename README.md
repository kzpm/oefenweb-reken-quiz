# CakePHP Rekenquiz



[![Build Status](https://img.shields.io/travis/cakephp/app/master.svg?style=flat-square)](https://travis-ci.org/cakephp/app)
[![Total Downloads](https://img.shields.io/packagist/dt/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)

Rekenquiz is a simple webapplication aimed at children aging 8-12 years old.

How to:

When entering the application at url/rekenquiz, users will be presented a layout with only one exercise at a time. There is no need to hurry. For now all 
exercises are time-independent.

The goal is to answer as many exercises as possible.

When hitting a high-score, the app calculates balance between the total number of exercises divided by the number of correct 
answers.

Example:
So if someone manages to perform 100 exercises and 65 of 100 were correct, the percentage correct answers is 65%.
The number of correct answers is divided by the total number of processed exercizes.
The new high score will be 0.65. So every score above 0.65 will be a new high score.

High scores are accessible, by clicking "Hi scores" in the right upper corner of the screen. A new browser tab, filled with scores, will now appear.
