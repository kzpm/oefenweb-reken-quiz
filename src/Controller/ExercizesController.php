<?php

    namespace App\Controller;
    session_start();
    use App\Controller\AppController;
    use App\Model\Table\ExercizesTable;
    use App\Model\Entityl\Exercize;
    use App\Model\Table\NicknamesTable;
    use App\Model\Entityl\Nickname;
    use Cake\I18n\Time;
    use Cake\ORM\TableRegistry;

    class ExercizesController extends AppController
    {

      //First we want to offer a user to enter his nickname, before playing starts

      public function index()
      {

        $this->loadComponent('Flash');
          // * Initialize variables that will be passed to index.ctp
          $operators=array('addition','substraction','multiplication','division');
          $first_number=rand(0,100);
          $second_number=rand(0,100);
          shuffle($operators);

          // Validation. Are variables within allowed ranges (0..100)?
          if ( $first_number < 101 && $first_number > -1 && $second_number < 101 && $second_number > -1){

              // Logic for substraction.
              if ( $operators[0] == 'substraction'  && ( $first_number > $second_number )){
                //Presentation in index.ctp
                $som = $first_number.'-'.$second_number;
                //Precalculate answer and pass it to the form in index.ctp
                $answ = $first_number-$second_number;
              }else{
                $som = $first_number.'+'.$second_number;
                $answ = $first_number+$second_number;
              }//end if

              // Logica voor het aanbieden van een deelsom
              if ( $operators[0] == 'division'  && ($second_number !== 0) && ( $first_number % $second_number == 0) ){
                $som = $first_number.':'.$second_number;
                $answ = $first_number/$second_number;
              }else{
                 $som = $first_number.'+'.$second_number;
                 $answ = $first_number+$second_number;
              }//end if

              // Logica voor het aanbieden van een vermenigvuldiging
              if ( $operators[0] == 'multiplication' && ( $first_number < 11 || $second_number <11) ){
                $som = $first_number.'x'.$second_number;
                $answ = $first_number*$second_number;
               }//end if

            // Logica voor het aanbieden van een optelling
            if ( $operators[0] == 'addition' ){
              $som = $first_number.'+'.$second_number;
              $answ=$first_number+$second_number;
            }//end if

            $this->set(compact('som'));
            $this->set(compact('answ'));
            //$_SESSION['answ'] = $answ;
    }//end if


  if($this->request->is('post')){

        // Execute `results` function when user clicks stop
        if ( isset( $_POST['stopMe'] )){

            $this->results();
        }
        // Update counters for correct and wrong answers
        if ( isset( $_POST['postMe'] ) && isset( $_POST["answ"]) && $_POST['postMe'] === $_POST['answ'] ){
                $this->correctcount(1);
        }else{
                if ( ! isset($_POST["stopMe"])){
                    $this->wrongcount(1);
              }
        }
        // init and update variable for execersize progress
        $totalanswered= $_SESSION['correctcount']+$_SESSION['wrongcount']=1;
        $this->set(compact('totalanswered'));

        // Clear stored values in post data
        $_POST=array();
     }
  }

// Actual increase of `correct` counter
public function correctcount($addcnt){
    $_SESSION['correctcount'] ++;
    $this->Flash->set('Goed');
}

// Actual increase of `wrong` counter
public function wrongcount($addcnt){
    $_SESSION['wrongcount'] ++;
    $this->Flash->set('Jammer');

}

// Create result overview. Activated when user clicks Stop button
public function results(){

        $totalanswered= $_SESSION['correctcount']+$_SESSION['wrongcount'];
        $goed=$_SESSION['correctcount'];
        $fout=$_SESSION['wrongcount'];

        //Prevent user from early stopping exercizes. User has to play 10 exercises
        if ($totalanswered > 1 ){

              $score=$goed/$totalanswered;
              echo('<div class="boxje">
              Je resultaat is:<br>
              Aantal antwoorden: '.$totalanswered.'<br>
              Aantal goed :'.$goed.'<br>
              Aantal fout:'.$fout.'<br>
              Score:'.number_format((float)$score, 2,'.',' ').'
              </div>'
                );
                $_SESSION['correctcount']=0;
                $_SESSION['wrongcount']=0;

                $this->savetotable( $score );
      }else{

              $this->Flash->set('Je moet minimaal 10 sommen maken');
      }

}

public function getscores(){

      $hiscores = TableRegistry::getTableLocator()->get('Exercizes');
      $query = $this->Exercizes
              ->find()
              ->select(['hs1'])
              ->order(['hs1' => 'DESC'])
              ->limit(5);
      $query = $this->set(['query' => $query]);
}

 public function savetotable( $score ){

     $date = array('Time');
      $hstable= $this->Exercizes->newEntity();
      $hstable->id =h($date);
      $hstable->hs1 = number_format((float)$score, 2,'.',' ');
      if( $score > 0 &&  $this->Exercizes->save($hstable) ){

          $this->Flash->set('Score is opgeslagen');

      }

 }

}//end class
